import yaml
from PIL import Image
import os
import numpy as np

def load_mapping(file_path):
    mapping = {}
    with open(file_path, 'r') as file:
        for line in file:
            parts = line.strip().split('\t')
            if len(parts) == 2:
                key, value = parts
                mapping[int(key)] = value
    return mapping

def lookup_description(number, mapping):
    return mapping.get(number, "Unknown")

def lookup_number(description, mapping):
    for key, value in mapping.items():
        if value == description:
            return key
    return None

def load_categories_mapping(yaml_file):
    with open(yaml_file, 'r') as file:
        data = yaml.safe_load(file)
    
    categories_mapping = {}
    
    for supercategory, subcategories in data['things'].items():
        for subcategory, items in subcategories.items():
            for item in items:
                categories_mapping[item] = subcategory

    for supercategory, subcategories in data['stuff'].items():
        for subcategory, items in subcategories.items():
            for item in items:
                categories_mapping[item] = subcategory
    
    return categories_mapping

def get_supercategory(category, categories_mapping):
    return categories_mapping.get(category, 'Unknown')


def get_categories_in_image(path, filename_image):
    image_path = f"{path}/{filename_image}"
    img = Image.open(image_path)
    rgb_values_list = []
    pixels = img.load() 
    #iterate over all pixels, save all different pixel values to list and change all pixel values to coarse values
    for i in range(img.size[0]):
        for j in range(img.size[1]):            
            if pixels[i,j] == 255:
                continue
            if pixels[i,j] not in rgb_values_list:
                rgb_values_list.append(pixels[i,j])

    img.close()
    return rgb_values_list

def crop_suffix(word, suffixes):
    for suffix in suffixes:
        if word.endswith(suffix):
            return word[:-len(suffix)].strip('-')
    return word


def create_prompts(path, name, path_to_original_files, path_to_annotated_coarse_files, path_to_annotated_fine_files, coarse_mapping, fine_mapping):
    file_folder = os.getcwd()
    superfolder_path = os.path.abspath(os.path.join(file_folder, '..'))  # Go up one level

    suffixes_to_remove = ['stuff', 'things']

    with open(f"{superfolder_path}/{path}/prompts_{name}.txt", "w") as f:
        f.write("")
        f.close()

    for filename in os.listdir(path_to_original_files):
        if filename.endswith('.jpg'): #to remove .DS_Store
            #get all labels for the current image
            label_numbers = []
            label_description = []
            if name == "coarse":
                label_numbers = get_categories_in_image(path_to_annotated_coarse_files, filename.replace('.jpg', '.png'))
                label_description = []
                for i in range(len(label_numbers)):
                    label_description.append(lookup_description(label_numbers[i], coarse_mapping))
            
                label_description = [crop_suffix(label, suffixes_to_remove) for label in label_description]
            
            if name == 'fine': 
                label_numbers = get_categories_in_image(path_to_annotated_fine_files, filename.replace('.jpg', '.png'))
                for i in range(len(label_numbers)):
                    label_description.append(lookup_description(label_numbers[i], fine_mapping))
            #remove duplicates in cropped_labels (bc. of food-stuff and food-things)
            label_description = list(set(label_description))

            if len(label_description) == 1:
                prompt = "an image of a " + label_description[0]
            else:
                # Use " and a " between all items except the last one
                prompt = "an image of a " + " and a ".join(label_description[:-1]) + " and a " + label_description[-1]
            
            #write prompt to txt file, one prompt per line, together with filename
            with open(f"{superfolder_path}/{path}/prompts_{name}.txt", "a") as f:
                f.write(f"{filename}: {prompt}\n")
                f.close()
            
            print(f"{filename}: {prompt}")

            print(prompt)

def store_png_image_as_512(input_folder_path, output_folder_path, image_name, left=0, right=0, top=0, bottom=0):
    image = np.array(Image.open(f"{input_folder_path}/{image_name}"))[:, :]
    h, w = image.shape
    left = min(left, w-1)
    right = min(right, w - left - 1)
    top = min(top, h - left - 1)
    bottom = min(bottom, h - top - 1)
    image = image[top:h-bottom, left:w-right]
    h, w = image.shape
    if h < w:
        offset = (w - h) // 2
        image = image[:, offset:offset + h]
    elif w < h:
        offset = (h - w) // 2
        image = image[offset:offset + w]

    #i needed to add Image.NEAREST s.t no interpolation happens
    image = Image.fromarray(image).resize((512, 512), resample=Image.NEAREST)
    image.save(f"{output_folder_path}/{image_name}")

def store_jpg_image_as_512(input_folder_path, output_folder_path, image_name, left=0, right=0, top=0, bottom=0):
    image = np.array(Image.open(f"{input_folder_path}/{image_name}"))[:, :, :3]
    h, w, c = image.shape
    left = min(left, w-1)
    right = min(right, w - left - 1)
    top = min(top, h - left - 1)
    bottom = min(bottom, h - top - 1)
    image = image[top:h-bottom, left:w-right]
    h, w, c = image.shape
    if h < w:
        offset = (w - h) // 2
        image = image[:, offset:offset + h]
    elif w < h:
        offset = (h - w) // 2
        image = image[offset:offset + w]

    #i needed to add Image.NEAREST s.t no interpolation happens
    image = Image.fromarray(image).resize((512, 512), resample=Image.NEAREST)
    image.save(f"{output_folder_path}/{image_name}")

def convert_fine_to_coarse_segmentation(path, output_folder_path, filename_image, labels_in_image_dict, fine_mapping, coarse_mapping, categories_mapping):

    # Specify the path to your PNG image
    image_path = f"{path}/{filename_image}"
    # Open the image
    img = Image.open(image_path)

    #display(img)

    #collect all different rgb values in a list
    rgb_values_list = []

    pixels = img.load() # create the pixel map
    
    #iterate over all pixels, save all different pixel values to list and change all pixel values to coarse values
     
    for i in range(img.size[0]): # for every pixel:
        for j in range(img.size[1]):            
            if pixels[i,j] == 255:
                continue
            
            #look up rgb_values[i] in the mapping
            description_of_pixel = lookup_description(pixels[i,j], fine_mapping)
            #print(description_of_pixel)
            supercategory = get_supercategory(description_of_pixel, categories_mapping)
            #print(supercategory)
            coarse_pixel_value = lookup_number(supercategory, coarse_mapping)
            #print(coarse_pixel_value)

            pixels[i,j] = coarse_pixel_value

            if coarse_pixel_value not in rgb_values_list:
                rgb_values_list.append(pixels[i,j])


    for k in range(len(rgb_values_list)):
        labels_in_image_dict[rgb_values_list[k]].append(filename_image)

    #print(rgb_values_list)
    #display(img)
    img.save(f"{output_folder_path}/{filename_image}")
    

    # Close the image
    img.close()

    return labels_in_image_dict

def get_categories_in_image_and_add_to_dict(path, filename_image, labels_in_image_dict):
    image_path = f"{path}/{filename_image}"
    img = Image.open(image_path)
    rgb_values_list = []
    pixels = img.load() 
    #iterate over all pixels, save all different pixel values to list and change all pixel values to coarse values
    for i in range(img.size[0]):
        for j in range(img.size[1]):            
            if pixels[i,j] == 255:
                continue
            if pixels[i,j] not in rgb_values_list:
                rgb_values_list.append(pixels[i,j])

    img.close()

    for k in range(len(rgb_values_list)):
        labels_in_image_dict[rgb_values_list[k]].append(filename_image)

    return labels_in_image_dict


