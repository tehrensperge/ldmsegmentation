# Download and prepare COCO-Stuff-27 dataset

This README tells you how to get the COCO Stuff dataset and how to get the same selection of images (subset of COCO Stuff) if you want to reproduce our results. This is not necessary if you just want to run the code on the same images as we did. Then, you can directly access the images which we included in the repository.

## How to get the COCO-Stuff-27 dataset locally

We followed the download instructions for the COCO Stuff dataset from the[ IIC repository](https://github.com/xu-ji/IIC) and [this](https://github.com/kazuto1011/deeplab-pytorch/blob/master/data/datasets/cocostuff/README.md) repository. We repost it here in the version which was most pleasant for us to get the dataset. We also copied the file setup_cocostuff164k.sh from the latter repository. Notice that we uncommented the download of the training set since no training is necessary for our experiments and we only work with the validation set.


1.  Run the script to download the datset (on same directory as this README file):
$ bash setup_cocostuff164k.sh [LOCAL PATH TO DOWNLOAD]

There should now be "annotations" and "images" within [LOCAL PATH TO DOWNLOAD].

We took the util files for the COCO Stuff dataset from the [IIC repository](https://github.com/xu-ji/IIC) (code/datasets/segmentation/util). These files are included in our repository and you don't need to copy them.

## How to get the same selection of COCO-Stuff-27 images as we did
1. Run coco_select_images.ipynb (change paths to local COCO Stuff dataset)
2. Run coco_create_prompts.ipynb (change paths to local COCO Stuff dataset)

