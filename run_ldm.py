##run LDM for a single image and save resulting files

import os
from dataclasses import dataclass
import torch
from torchvision.transforms import ToTensor
from torchvision.utils import save_image
import numpy as np
from local_prompt_mixing.src.attention_based_segmentation import collect_self_attention, collect_cross_attention
from local_prompt_mixing.src.diffusion_model_wrapper import get_stable_diffusion_model, get_stable_diffusion_config, DiffusionModelWrapper
from local_prompt_mixing.src.null_text_inversion import invert_image
from local_prompt_mixing.src.prompt_to_prompt_controllers import AttentionStore
from src.segmentation_utils import load_512, save_text


@dataclass
class SegmentationConfig:
    seed: int = 1111
    gpu_id: int = 0
        
    auth_token: str = ""
    low_resource: bool = False
    num_diffusion_steps: int = 50
    guidance_scale: float = 7.5
    max_num_words: int = 77
    
    export_decoder = False

    num_segments: int = 5
    background_segment_threshold: float = 0.35


def self_attention_file(exp_path): return f"{exp_path}/self_attention.npy"
def cross_attention_file(exp_path): return f"{exp_path}/cross_attention.npy"
def real_image_file(exp_path): return f"{exp_path}/real_image.jpg"
def all_latents_file(exp_path): return f"{exp_path}/all_latents.npy"
def prompt_file(exp_path): return f"{exp_path}/prompt.txt"

def open_real_image(exp_path):
    return load_512(real_image_file(exp_path))


#return all filenames produced by this algorithm, exactly: all_latents, clusters, clusters_merged & reconstructed image
def all_output_filenames(exp_path, no_prompts):
    if no_prompts:
        return [
            all_latents_file(exp_path),
            f"{exp_path}/image_rec.jpg",
            self_attention_file(exp_path),
            prompt_file(exp_path)
        ]
    else:
        return [
            all_latents_file(exp_path),
            f"{exp_path}/image_rec.jpg",
            self_attention_file(exp_path),
            cross_attention_file(exp_path), #not necessary for no prompt version...
            prompt_file(exp_path)
        ]

#return true if all the result files exist
def ldm_done(exp_path, no_prompt):
    return all(map(os.path.exists, all_output_filenames(exp_path, no_prompt)))


#run ldm from the localizing prompt mixing paper
#use the image with the given path, the given prompt, and export folder path
#this file only produces the latents & attentions. Another source is responsible for performing the clustering
def main(image_path, prompt, exp_path, no_prompts, verbose = True):
    #load default arguments
    args = SegmentationConfig()
    
    #replace arguments with defaults if none are provided
    if image_path is None: image_path = args.real_image_path
    if prompt is None: prompt = args.prompt
    if exp_path is None: exp_path = args.exp_path

    #if all segmentation results are already present, return
    if ldm_done(exp_path, no_prompts):
        if verbose: print (f"All of the segmentation results are already present, segmentation is not running with image_path={image_path}, prompt={prompt}, exp_path={exp_path}")
        return

    #create the results directory
    os.makedirs(exp_path, exist_ok=True)

    #get the filenames of all the results
    if no_prompts:
        all_latents_filename, img_reconstruction_filename, self_attention_filename, prompt_file = all_output_filenames(exp_path, no_prompts)
    else:
        all_latents_filename, img_reconstruction_filename, self_attention_filename, cross_attention_filename, prompt_file = all_output_filenames(exp_path, no_prompts)

    save_text(prompt_file, prompt)

    #load the LDM models
    ldm_stable = get_stable_diffusion_model(args)
    ldm_stable_config = get_stable_diffusion_config(args)

    
    x_t = None
    uncond_embeddings = None
    #if there is a real image to perform the segmentation of, attempt to reconstruct the diffusion process that would generate it
    if image_path != "":
        x_t, uncond_embeddings = invert_image(image_path, ldm_stable, ldm_stable_config, [prompt], exp_path)

    g_cpu = torch.Generator(device=ldm_stable.device).manual_seed(args.seed)
    controller = AttentionStore(ldm_stable_config["low_resource"])
    diffusion_model_wrapper = DiffusionModelWrapper(args, ldm_stable, ldm_stable_config, controller, generator=g_cpu, export_dir=exp_path if args.export_decoder else None)
    #run the forward pass
    image, x_t, orig_all_latents, _ = diffusion_model_wrapper.forward([prompt],
                                                                      latent=x_t,
                                                                      uncond_embeddings=uncond_embeddings)
    
    #save all the latents created during the generation
    np.save(all_latents_filename, torch.cat([l.cpu() for l in orig_all_latents], 0))


    np.save(self_attention_filename, collect_self_attention(controller, prompt).cpu())
    if not no_prompts:
        np.save(cross_attention_filename, collect_cross_attention(controller, prompt).cpu())
    
    #save the reconstructed image
    save_image(ToTensor()(image[0]), img_reconstruction_filename)



#if this is the main file, just run with the default parameters
if __name__ == '__main__':
    real_image_path = "/Users/TimUni/Desktop/ldmsegmentation/data/demo/val2017_original/rinon_cat.jpg"
    default_prompt = ""
    default_exp_path = "demo_show_results"
    no_prompts = True

    main(real_image_path, default_prompt, default_exp_path, no_prompts)