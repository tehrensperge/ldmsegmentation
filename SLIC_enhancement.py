#OUR METHOD 1: uses SLIC algorithm on real image to produce high-dimensional clusters and assigns each segment a class from low-dimensional cluster via voting

import run_ldm
import os
from skimage.segmentation import slic
import numpy as np
import kmeans_clustering
from simple_enhancement import segmentation_voting

LINEAR = 1

#Use SLIC to enhance segmentation quality
# -> = Load the clusters & the image, run SLIC on the image, assign each of the resulting clusters one of the classes present in clusters (the one most present on the region)
def slic_enhancement(exp_path, cluster_count, slic_segments):
    result_fname = f"{exp_path}/linear_c{cluster_count}_s{slic_segments}.npy"
    if os.path.exists(result_fname): return np.load(result_fname)

    clusters = np.load(kmeans_clustering.clusters_file(exp_path, cluster_count))

    #load the image and run SLIC
    image = run_ldm.open_real_image(exp_path)
    slic_segments = slic(image, n_segments = slic_segments, sigma = 5)

    #perform voting -> assign each SLIC segment a class
    result = segmentation_voting(clusters, LINEAR, slic_segments)
    np.save(result_fname, result)

    return result