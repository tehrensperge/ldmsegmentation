import numpy as np

import pickle
import matplotlib.pyplot as plt
import argparse

from PIL import Image
import os


def load_512(fname):
    image = np.array(Image.open(fname))
    if len(image.shape) == 3: image = image[:, :, :3]

    h, w = image.shape[:2]
    left = min(0, w-1)
    right = min(0, w - left - 1)
    top = min(0, h - left - 1)
    bottom = min(0, h - top - 1)
    image = image[top:h-bottom, left:w-right]
    h, w = image.shape[:2]
    if h < w:
        offset = (w - h) // 2
        image = image[:, offset:offset + h]
    elif w < h:
        offset = (h - w) // 2
        image = image[offset:offset + w]
    image = np.array(Image.fromarray(image).resize((512, 512)))
    return image

def save_image(fname, image):
    Image.fromarray(image).save(fname)


#save the cluster2noun dictionary to a file
def save_dict(file, c2n):
    with open(file, "wb") as f:
        pickle.dump(c2n, f)

#load the saved cluster2noun dict
def load_dict(file):
    with open(file, "rb") as f:
        c2n = pickle.load(f)
    return c2n

#save the given string to file
def save_text(file, data):
    with open(file, 'w') as f:
        f.write(data)


def load_text(file):
    return open(file).read()


#perform a label segmentation - given clusters and their assigned labels, return a segmentation where the clusters with the same segmentation are merged
def label_segmentation(label_img, cluster2noun):
    #original label count
    label_count = np.max(label_img)+1
    
    #we want to create a mapping from old labels to new ones (where the clusters with the same label are merged)
    mapping = np.zeros([label_count])
    #dictionary, mapping element -> new cluster index
    cur_map = {}
    #go over all cluster descriptions
    for i, (_, n) in enumerate(cluster2noun.items()):
        #if this description was present before, just set the mapping to point to the same cluster
        if n in cur_map:
            mapping[i] = cur_map[n]
        #description appeared for the first time - assign a new cluster number to it
        else:
            mapping[i] = len(cur_map)
            cur_map[n] = len(cur_map)
    #convert image to the new clusters
    return mapping[label_img]
    

#Mark all clustering pixels that are neighbors with a different color
def interesting_pixels(clusters):
    sizex, sizey = clusters.shape
    #the shifts we perform and check for inequality - right now, we check the 3x3 surrounding region
    shifts = [(dx, dy) for dx in [-1, 0, 1] for dy in [-1, 0, 1] if dx != 0 or dy != 0]
    interesting = np.zeros_like(clusters, bool)
    for dx, dy in shifts:
        #find the comparison region - ...1 is in the original image, ...2 is the shifter version
        from_x1, from_y1, to_x1, to_y1 = max(0, dx), max(0, dy), min(sizex, sizex+dx), min(sizey, sizey+dy)
        from_x2, from_y2, to_x2, to_y2 = max(0,-dx), max(0,-dy), min(sizex, sizex-dx), min(sizey, sizey-dy)
        #update interesting pixels if the values changed between the current & shifted version
        interesting[from_x1:to_x1, from_y1:to_y1] |= clusters[from_x1:to_x1, from_y1:to_y1] != clusters[from_x2:to_x2, from_y2:to_y2]
    return interesting

#nearest neighbors resize
def resize_img_nns(image, shape):
    #shape the same - nothing changes
    if image.shape[0] == shape[0] and image.shape[1] == shape[1]: return image
    #the resulting image
    result = np.zeros(shape, dtype=np.int32)
    for x in range(shape[0]):
        for y in range(shape[1]):
            #save one target pixel
            result[x, y] = image[(x * image.shape[0]) // shape[0], (y * image.shape[1]) // shape[1]]
    return result

#return an image where the cluster value differs from the neighbors -> shape [N, 2]
def interesting_pixels_resized(shape, clusters):
    #find interesting positions
    pix = interesting_pixels(clusters)
    #resize them to the required shape
    return resize_img_nns(pix, shape)



def load_any(fname):
    ext = fname.split(".")[-1]
    match ext:
        case "npy": return np.load(fname)
        case "pkl": return load_dict(fname)
        case "jpg": return load_512(fname)
        case "png": return load_512(fname)
        case _:
            print (f"Attempted to load an unsupported file: {fname}")
            return None
        
def save_any(fname, data):
    ext = fname.split(".")[-1]
    match ext:
        case "npy": return np.save(fname, data)
        case "pkl": return save_dict(fname, data)
        case "jpg": return save_image(fname, data)
        case "png": return save_image(fname, data)
        case _:
            print (f"Attempted to save an unsupported file: {fname}")
            return None
            
def shift_labels(image):
    ### 
    unique_values = np.unique(image)
    mapping = {v:i for i, v in enumerate(unique_values)}

    new_clusters = np.zeros_like(image)
    for x in range(image.shape[0]):
        for y in range(image.shape[1]):
            new_clusters[x, y] = mapping[image[x, y]]
    return new_clusters
    # unique_values = np.unique(image)
    # missing_values = set(range(np.min(unique_values), np.max(unique_values) + 1)) - set(unique_values)

    # for missing_value in sorted(missing_values, reverse=True):
    #     image = np.where(image > missing_value, image - 1, image)

    # return image


parser = argparse.ArgumentParser()
parser.add_argument("--exp_path", type=str, default = "segmentation_results_cat", help="The path from which to read images")

if __name__ == "__main__":
    args = parser.parse_args()
    #load the cluster2noun file for the given exp_path. Careful - these are not all in git, I didn't rerun the segmentation again :(
    cluster2noun = load_cluster2noun(args.exp_path)
    #load the current clusters
    clusters = np.load(f"{args.exp_path}/clusters.npy")
    #merge the clusters in accordance to the cluster2noun
    clusters_merged = label_segmentation(clusters, cluster2noun)
    
    #final visualization - 3 graphs
    fig, (a, b, c) = plt.subplots(1, 3)
    
    #first image - show the original clusters
    a.imshow(clusters)
    a.set_title("Clusters")
    
    #second image - show the merged clusters
    b.imshow(clusters_merged)
    b.set_title("Merged clusters")
    
    #third image - show the interesting pixels (one neighboring with a different cluster) in the final image
    c.imshow(interesting_pixels(clusters_merged))
    c.set_title("Interesting pixels")
    
    plt.show()

