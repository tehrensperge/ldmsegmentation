import numpy as np
from skimage.segmentation import slic

### Main function
def SLIC_enhancement(original_image, latent_clusters, slic_nr_segments, interpolation_method='linear'):
        """
        :param interpolation_method: {'nearest', 'linear'}. nearast neighbor or linear interpolation (linear seemed to always work better)
        """
        
        #create the given amount of SLIC segments
        slic_segments = slic(original_image, slic_nr_segments, sigma = 5)
        
        #compute segmentation (nearest neigbors are nearly always worse so just use linear intepolation)
        full_segmentation_lin = enhance_segmentation(original_image, slic_segments, latent_clusters, interpolation_method)
        #some clusters might be ignored. make sure the labels stay correct (for future methods)
        full_segmentation_lin = shift_labels(full_segmentation_lin)

        return full_segmentation_lin


### Helper functions
def enhance_segmentation(image, slic_segments, lowres_segments, interpolation_method):    
    #for each slic segment, collect votes for each lowres segment. The segment most present in this area will be deemed to fill the whole area in the end 
    votes = np.zeros([np.max(slic_segments)+1, np.max(lowres_segments)+1], dtype=np.int32)
    
    #go over all image pixels
    for x in range(image.shape[0]):
        for y in range(image.shape[1]):
            slic_seg = slic_segments[x, y]
            if interpolation_method == 'nearest':
                #nearest neigbor - the value of the current pixel is equal to the nearest lowres segment - just add one vote for it
                lr_seg_nn = lowres_segments[x // 16, y // 16]
                
                votes[slic_seg, lr_seg_nn] += 1
            elif interpolation_method == 'linear':
                #linear interpolation between the four surrounding pixels
                
                # compute indices of the surrounding pixels (for x and y axes, unclamped)
                x0_, y0_ = (x - 8) // 16, (y - 8) // 16
                x1_, y1_ = x0_ + 1, y0_ + 1
                
                #clamp indices to image size
                x0, y0, x1, y1 = max(0, x0_), max(0, y0_), min(x1_, lowres_segments.shape[0]-1), min(y1_, lowres_segments.shape[1]-1)
                #get lowres segment labels of the 4 surrounding pixels
                a, b, c, d = lowres_segments[x0, y0], lowres_segments[x1, y0], lowres_segments[x0, y1], lowres_segments[x1, y1]
                
                #compute coefficients for each side
                kx1, ky1 = x1_ * 16 - (x - 8), y1_ * 16 - (y - 8)
                kx2, ky2 = 16-kx1, 16-ky1
                
                #add votes according to the linear interpolation
                votes[slic_seg, a] += kx1 * ky1
                votes[slic_seg, b] += kx2 * ky1
                votes[slic_seg, c] += kx1 * ky2
                votes[slic_seg, d] += kx2 * ky2
    
    #use the votes to select the most popular lowres segment for each slic region    
    slic_to_lr = np.argmax(votes, axis=1)

    #construct the high resolution segmentation image
    full_segmentation = np.zeros(image.shape[:2], dtype=np.int32)
    #go over all pixels
    for x in range(image.shape[0]):
        for y in range(image.shape[1]):
            #the seg pixel = set according to the color of the SLIC segment present here
            full_segmentation[x, y] = slic_to_lr[slic_segments[x, y]]
    
    return full_segmentation

def shift_labels(image):
    unique_values = np.unique(image)
    missing_values = set(range(np.min(unique_values), np.max(unique_values) + 1)) - set(unique_values)

    for missing_value in sorted(missing_values, reverse=True):
        image = np.where(image > missing_value, image - 1, image)

    return image   