import numpy as np
import matplotlib.pyplot as plt


def plot_segmentations(all_segmentations, true_segmentation, original_image):
    nr_cols = len(all_segmentations) + 1
    fig, axs = plt.subplots(2, nr_cols)

    axs[0,0].imshow(original_image)
    axs[0,0].set_title(f"Original image")
    axs[0,0].axis("off")

    axs[1,0].imshow(true_segmentation)
    axs[1,0].set_title(f"True segmentation")
    axs[1,0].axis("off")

    for nr_col, (name, clustered_image) in zip(range(nr_cols), all_segmentations.items()):
        score = compute_scores(true_segmentation, clustered_image)
        accuracy = np.round(score['accuracy'], 3)
        iou = np.round(score['iou'], 3)

        axs[0, nr_col+1].imshow(clustered_image)
        axs[0, nr_col+1].set_title(name)
        axs[0, nr_col+1].axis("off")

        axs[1, nr_col+1].imshow(clustered_image == true_segmentation)
        axs[1, nr_col+1].set_title(f"Correct segmentation\nAcc: {accuracy}\n IOU: {iou}")
        axs[1, nr_col+1].axis("off")

    plt.show()

def compute_scores (true_labels, pred_labels):
    #accuracy = what part of the pred labels is equal to the true labels
    accuracy = np.average(true_labels == pred_labels)
    scores = {"accuracy":accuracy}
    
    #how many labels do we have to go over
    max_label = max(np.max(true_labels), np.max(pred_labels))

    #aggregate IOU over all classes. Contributions weighed by sizes in true image
    total_iou = 0
    num_classes = 0

    #go over all the labels
    for i in range(max_label+1):
        #boolean arrays showing whether labels are equal or not
        tl, pl = true_labels == i, pred_labels == i

        #compute intersection and union of the two boolean fields
        intersection = np.logical_and(tl, pl)
        union = np.logical_or(tl, pl)

        #size of union is 0 -> this label isn't present -> skip
        if np.sum(union) == 0: continue

        #compute iou otherwise
        iou = np.sum(intersection) / np.sum(union)
        #scores[f"iou_{i}"] = iou

        #average = what part of pixels in the true image are assigned to this class -> we compute the average iou for all pixels
        total_iou += iou
        num_classes += 1
    scores["iou"] = total_iou / num_classes
    return scores

def add_scores_to_total_scores(all_predictions, true_segmentation, scores):
    #for first image, create dictionary entries with empty list for both accuracy and iou
    if scores == {}:
        for name, clustering in all_predictions.items():
            scores[name] = {"accuracy": [], "iou": []}

    #compute the IOU & accuracy scores
    for name, clustering in all_predictions.items():
        single_score = compute_scores(true_segmentation, clustering)
        scores[name]['accuracy'].append(single_score['accuracy'])
        scores[name]['iou'].append(single_score['iou'])
    
    return scores

def add_mean_scores(scores, all_predictions):
    for name, clustering in all_predictions.items():
        scores[name]['accuracy_mean'] = np.mean(scores[name]['accuracy'])
        scores[name]['accuracy_std'] = np.std(scores[name]['accuracy'])
        scores[name]['iou_mean'] = np.mean(scores[name]['iou'])
        scores[name]['iou_std'] = np.std(scores[name]['iou'])