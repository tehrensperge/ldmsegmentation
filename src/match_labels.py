##hungarian, argmax and cross attention matching to assign a COCO label to a cluster

import numpy as np
import scipy
import nltk

def match_labels(clustered_image, true_segmentation, method, cross_attention_method='median', latent_clusters=None, cross_attention=None, prompt=None, background_segment_threshold=0.35, coco_version='coarse', data_path=None):
    """
    :param clustered_image: 512x512 {0, 1, .. nr_clusters-1}
    :param method: {'hungarian', 'argmax', 'cross_attention'}
    :param cross_attention_method: {'median', 'majority'}
    """
    if method == 'hungarian':
        clustered_image_matched = hungarian_matching(clustered_image, true_segmentation)
    if method == 'argmax':
        clustered_image_matched = argmax_matching(clustered_image, true_segmentation)
    if method == 'cross_attention':
        if cross_attention_method == 'median':
            cluster2noun = match_cluster2cross_attention(latent_clusters, cross_attention, prompt, background_segment_threshold)
        if cross_attention_method == 'majority':
            cluster2noun = match_cluster2cross_attention_majority(latent_clusters, cross_attention, prompt, background_segment_threshold)
        clustered_image_matched = cross_attention_matching(clustered_image, cluster2noun, coco_version, data_path)

    return clustered_image_matched

### Functions to convert clusters to COCO labels:
# using hungarian matching algorithm needs nr_clusters <= nr_true clusters
def hungarian_matching(clustered_image, true_segmentation):
    labels_model = np.unique(clustered_image)
    labels_true = np.unique(true_segmentation)

    if len(labels_model) > len(labels_true): return None

    cost_matrix = np.zeros((len(labels_model), len(labels_true)))

    for i, true_label in enumerate(labels_true):
        for j, model_label in enumerate(labels_model):
            test_cluster = np.copy(clustered_image)
            test_cluster[test_cluster != model_label] = -10
            test_cluster[test_cluster == model_label] = true_label
            cost_matrix[j, i] = np.mean(test_cluster == true_segmentation)

    row_ind, col_ind = scipy.optimize.linear_sum_assignment(cost_matrix, True)

    labels_ordered = labels_true[col_ind]
    new_cluster = labels_ordered[clustered_image]

    return new_cluster

# directly assign biggest accuracy
def argmax_matching(clustered_image, true_segmentation, in32=False, cluster32=None):
    labels_model = np.unique(clustered_image)
    labels_true = np.unique(true_segmentation)

    cost_matrix = np.zeros((len(labels_model), len(labels_true)))

    for i, true_label in enumerate(labels_true):
        for j, model_label in enumerate(labels_model):
            test_cluster = np.copy(clustered_image)
            test_cluster[test_cluster != model_label] = -10
            test_cluster[test_cluster == model_label] = true_label
            cost_matrix[j, i] = np.mean(test_cluster == true_segmentation)

    assign = labels_true[np.argmax(cost_matrix, 1)]

    new_cluster = assign[clustered_image]

    if in32 and cluster32 is not None:
        new_cluster = assign[cluster32]

    return new_cluster

### Cross attention method
def cross_attention_matching(clustered_image, cluster2noun, coco_version, data_path):
    _, n2label = load_coco_labels(coco_version, data_path)

    #conversion array of cluster indices to coco indices. Is 255 (no label) by default
    cluster2coco = np.full(len(cluster2noun), 255, np.int32)

    #go over all items in cluster2noun
    for i, l in cluster2noun.items():
        #if this cluster is associated to a word and not to background
        if l != 'BG':
            #get the associated noun, if it is registered by coco, assign the given label
            noun = l[1]
            if noun in n2label:
                cluster2coco[i] = n2label[noun]
            else:
                print (f"Couldn't find coco label for '{noun}'")

    #convert the segmented image into coco labels
    new_cluster = cluster2coco[clustered_image]

    return new_cluster

### Helper methods for cross_attention method
#load coco labels. coc_version can be either 'fine' or 'coarse' -> the file 'data/coco_utilc/cocostuff_{coc_version}_raw.txt' will be accessed
def load_coco_labels(coco_version, data_path):
    # the dictionaries to fill (conversion label int -> noun & noun -> label int)
    label2noun = {}
    noun2label = {}
    #go over all lines in the file
    for l in open(f"{data_path}/coco_utils/cocostuff_{coco_version}_raw.txt").read().split("\n"):
        #if the line is empty, skip
        if l == "": continue
        #split into the integer index & the label
        i, noun = l.split('\t')
        #save values
        label2noun[i] = noun
        noun2label[noun] = i
    return label2noun, noun2label

def match_cluster2cross_attention(latent_clusters, cross_attention, prompt, background_segment_threshold):
    num_segments = len(np.unique(latent_clusters))

    # create list of words
    tokenized_prompt = nltk.word_tokenize(prompt)
    nouns = [(i, word) for (i, (word, pos)) in enumerate(nltk.pos_tag(tokenized_prompt)) if pos[:2] == 'NN']

    cluster2noun = {}
    nouns_indices = [index for (index, word) in nouns]
    nouns_maps = cross_attention[:, :, [i + 1 for i in nouns_indices]]
    normalized_nouns_maps = np.zeros_like(nouns_maps).repeat(2, axis=0).repeat(2, axis=1)
    for i in range(nouns_maps.shape[-1]):
        curr_noun_map = nouns_maps[:, :, i].repeat(2, axis=0).repeat(2, axis=1)
        normalized_nouns_maps[:, :, i] = (curr_noun_map - np.abs(curr_noun_map.min())) / curr_noun_map.max()
    for c in range(num_segments):
        cluster_mask = np.zeros_like(latent_clusters)
        cluster_mask[latent_clusters == c] = 1
        score_maps = [cluster_mask * normalized_nouns_maps[:, :, i] for i in range(len(nouns_indices))]
        scores = [score_map.sum() / cluster_mask.sum() for score_map in score_maps]
        score_median = [np.median(score_map[np.where(cluster_mask)]) for score_map in score_maps]
        cluster2noun[c] = nouns[np.argmax(np.array(score_median))] if max(scores) > background_segment_threshold else "BG"
    return cluster2noun

# use majority voring instead
def match_cluster2cross_attention_majority(latent_clusters, cross_attention, prompt, background_segment_threshold):
    num_segments = len(np.unique(latent_clusters))

    # create list of words
    tokenized_prompt = nltk.word_tokenize(prompt)
    nouns = [(i, word) for (i, (word, pos)) in enumerate(nltk.pos_tag(tokenized_prompt)) if pos[:2] == 'NN']

    cluster2noun = {}
    nouns_indices = [index for (index, word) in nouns]
    nouns_maps = cross_attention[:, :, [i + 1 for i in nouns_indices]]
    normalized_nouns_maps = np.zeros_like(nouns_maps).repeat(2, axis=0).repeat(2, axis=1)
    for i in range(nouns_maps.shape[-1]):
        curr_noun_map = nouns_maps[:, :, i].repeat(2, axis=0).repeat(2, axis=1)
        normalized_nouns_maps[:, :, i] = (curr_noun_map - np.abs(curr_noun_map.min())) / curr_noun_map.max()
    for c in range(num_segments):
        cluster_mask = np.zeros_like(latent_clusters)
        cluster_mask[latent_clusters == c] = 1
        score_maps = np.array([cluster_mask * normalized_nouns_maps[:, :, i] for i in range(len(nouns_indices))])
        scores = np.argmax(score_maps, axis=0)
        score = np.argmax(np.bincount(scores[np.where(cluster_mask)]))
        cluster2noun[c] = nouns[score] if score > background_segment_threshold else "BG"
    return cluster2noun