import numpy as np
from sklearn.cluster import KMeans

def self_attention_segmentation(self_attention, nr_clusters):
    """
    Computes the segmentation based on the self attention during the diffusion process. Here with K-means.

    :param self_attention: np.array() of shape (32, 32, 32^2). each of the 3rd axis vector represents the attention to all other pixels
    :param nr_clusters: int. how many clusters there should be

    :return np.array() of shape (32, 32) consisting of values {0, 1, .., nr_cluster}
    """
    np.random.seed(1)
    kmeans = KMeans(n_clusters=nr_clusters).fit(self_attention.reshape(1024, 1024))
    clusters = kmeans.labels_

    return clusters.reshape(32, 32)