##main file. computes and displays the scores for the test files in `data`, using the `results` files and true COCO-Stuff-27 annotations

import run_everything
from src.match_labels import match_labels
from src.segmentation_utils import load_512, resize_img_nns, shift_labels
from src.plot_score_segmentations import compute_scores
import differential_enhancement
import numpy as np
import matplotlib.pyplot as plt
from skimage.segmentation import mark_boundaries
import csv



def main():
    #path where true files are located -> you need folder val2017_original in that folder, containing all the images
    data_path = "data/selected_coco_images"
    #path where result files of LDM and segmenation are stored
    results_path = "results"

    #"coarse" or "fine". the paper discusses the COCO-Stuff-27 version which is "coarse". "fine" has more labels, results are comparable and not displayed in the paper
    coco_version = "coarse"
    #True or False. paper mainly discusses True (no prompts used as input to LDM). if you want to match predicted clusters with cross-attention to true labels, use False
    no_prompts = True
    #list with different K parameters used for K-means clustering of self-attention 
    cluster_counts = [6, run_everything.TRUE_CLUSTER_COUNT]
    #list containing different number of segments used for SLIC superpixels method
    slic_segment_counts = [100]
    #list containing different versions of the second method presented in the paper (Jacobian of VAE). in the paper we only discuss the basic method differential_enhancement.SCORE_ENCODER since it's the most feasible to run
    differential_enhancements = [differential_enhancement.SCORE_ENCODER]
    #differential_enhancements = []
    #True or False. boolean if you want to run Method 3 from the paper (VAE decoder)
    run_vae_decoder_enhancement = True

    export_mean_scores_to_csv = True 

    #when true, all segmentation results are plotted. This results in about 5 plots each step
    plot_results_all = False

    #only plot the results that will be shown in the paper. One plot each step with 5 methods and the true segmentation
    plot_results_paper = False
    #show only the latent space - just clusters, not image & red lines
    plot_paper_only_latent = False

    #for each method & metric, the list of all achieved scores
    score_lists = {}

    skip_missing_differential_files = True
    
    #perform the required segmentations for all of the coco files
    for i, (args, result) in enumerate(run_everything.run_everything(data_path, results_path, coco_version, no_prompts, cluster_counts, slic_segment_counts, differential_enhancements, run_vae_decoder_enhancement, skip_missing_differential_files=skip_missing_differential_files)):
        print (f"Compute scores: image {i}")
        
        #real image we are performing the segmentation on
        orig_image = load_512(args.image)

        #ground truth image.
        true_labels = load_512(args.true)

        #list of all scores achieved
        scores = {}

        #go over all method results
        for cluster_method_name, clusters in result.items():
            #resize using nearest neighbors if the shape isn't 512x512 already
            if clusters.shape[0] != 512: clusters = resize_img_nns(clusters, (512, 512))

            if True:#no_prompts:
                #shift labels -> remove missing cluster indices from the image. the result will be the same clusters numbered [1, 2, .., N]
                clusters = shift_labels(clusters)
                #go over all scoring methods - argmax or hungarian
                for score_method in ['argmax', 'hungarian']:
                    #match labels -> convert our labels to the COCO representation using one of the strategies
                    matched = match_labels(clusters, true_labels, score_method)
                    #matching failed completely - skip this example, we cannot compute scores
                    if matched is None: continue
                    #add all scores for current method to the dict
                    scores |= {f"{score_method}_{cluster_method_name}_{n}":(value, matched) for n, value in compute_scores(true_labels, matched).items()}
            # else:
            #     # TODO - how to find the number of clusters (for loading all the files)?
            #     for cross_method in ['median', 'majority']:
            #         matched = match_labels(clusters, true_labels, 'cross_attention', cross_method, )


        #go over all scores computed in this step
        #add the current score to the list if it was measured before, otherwise create a new list
        for n, (score, _) in scores.items():
            if n in score_lists: score_lists[n].append(score)
            else: score_lists[n] = [score]
        
        #we want to plot all the results
        if plot_results_all:
            #go over everything
            for i, (method, (value, labels)) in enumerate(scores.items()):
                # for each 4 results, we want to plot one graph
                j = i % 4
                #before first image, create it
                if j == 0:        
                    fig, axs = plt.subplots(2, 4)
                
                #plot the current image
                axs[0][j].set_title(method)
                axs[0][j].imshow(mark_boundaries(orig_image, labels))
                axs[1][j].set_title(str(value))
                axs[1][j].imshow(true_labels == labels)

                #after last image out of 4, show the plot
                if j == 3:
                    plt.show()

        #plot results to show in the paper - 6 images in two rows, including the true segmentation
        if plot_results_paper:
            #create subplots & ravel the output array
            fig, axs = plt.subplots(2, 3)
            axs = [ax for axl in axs for ax in axl]

            #use the first plot to show the true labels
            axs[0].imshow(shift_labels(true_labels) if plot_paper_only_latent else mark_boundaries(orig_image, true_labels, color=(1,0,0), mode="thick"))
            axs[0].axis('off')
            axs[0].text(10, 60, "true", color='white', fontsize=30)

            #plot 5 methods, all of them on 6 clusters
            for i, name in enumerate(['nns_c6', 'linear_c6', 'slic_c6_s100', 'differential_c6_encoder', 'vae_enhanced_c6']):
                if name in result:
                    axs[i+1].imshow(result[name] if plot_paper_only_latent else mark_boundaries(orig_image, result[name], color=(1,0,0), mode='thick'))
                axs[i+1].text(10, 60, chr(ord('a')+i) + ')', color='white', fontsize=30)
                axs[i+1].axis('off')
            #only include small spacing between plots
            plt.subplots_adjust(wspace=0.05, hspace=0.05)
            plt.show()

    #compute the average score for all combinations of metric & method, print them
    average_scores = {n:np.mean(s) for n, s in score_lists.items()}
    print (f"Average Scores:{average_scores}")

    #if we want to export the scores to a CSV file
    if export_mean_scores_to_csv:
        prompts = "no_prompts" if no_prompts else "with_prompts"
        csv_file_path = f'average_scores_{coco_version}_{prompts}.csv'

        #open it & write everything
        with open(csv_file_path, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['Metric', 'Score'])  # Writing header

            for metric, score in average_scores.items():
                writer.writerow([metric, score])

        print(f"CSV file '{csv_file_path}' has been created.")

if __name__ == "__main__":
    main()