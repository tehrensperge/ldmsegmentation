##run LDM, segmentation and display results for all images in chosen folder

import run_ldm
import kmeans_clustering
import simple_enhancement
import differential_enhancement
import SLIC_enhancement
import VAEdecoder_enhancement
import coco_segmentation
import numpy as np
from src.segmentation_utils import load_512
import matplotlib.pyplot as plt

#use the number of clusters in the true label
TRUE_CLUSTER_COUNT = -1

#A generator that iterates over all images, for each it runs all of the methods and return results in a dictionary
def run_everything(data_path, results_path, coco_version, no_prompts, cluster_counts, slic_segment_counts, differential_enhancements, run_vae_decoder_enhancement, verbose=False, skip_missing_differential_files = False):

    #go over all the coco images
    for args in coco_segmentation.get_coco_files(data_path, results_path, coco_version, no_prompts):
        #get the true number of classes (unique labels present)
        true_cc = np.unique(load_512(args.true).ravel()).shape[0]

        result = {}
        #run the basic segmentation
        run_ldm.main(args.image, args.prompt, args.exp_path, no_prompts, verbose)
        #go over all of the cluster counts, for each
        for c in cluster_counts:
            #handle the case when c == TRUE_CLUSTER_COUNT
            c_name, c = ("true", true_cc) if c == TRUE_CLUSTER_COUNT else (str(c), c)

            #compute the KMeans clusters
            kmeans_clustering.main(args.exp_path, c, no_prompts)

            #enhance segmentation using nearest neighbors
            result[f"nns_c{c_name}"] = simple_enhancement.nearest_neighbors_enhancement(args.exp_path, c)

            #enhance segmentation using linear interp
            result[f"linear_c{c_name}"] = simple_enhancement.linear_enhancement(args.exp_path, c)

            #enhance segmentation using slic
            for s in slic_segment_counts:
                result[f"slic_c{c_name}_s{s}"] = SLIC_enhancement.slic_enhancement(args.exp_path, c, s)

        #differential segmentation enhancements - go over all methods we want to use
        for de in differential_enhancements:
            #compute the ehnanced scores for all of the images 
            enhanced = differential_enhancement.differential_enhancement(args.exp_path, de, False, [true_cc if c == TRUE_CLUSTER_COUNT else c for c in cluster_counts], verbose=verbose, skip_missing_differential_files=skip_missing_differential_files)
            
            #none was returned - segmentation was skipped to not block scores computation
            if enhanced is None: continue
            
            #save each of the results
            for c, e in zip(cluster_counts, enhanced):
                c_name, c = ("true", true_cc) if c == TRUE_CLUSTER_COUNT else (str(c), c)
                result[f"differential_c{c_name}_{differential_enhancement.score_fnames[de]}"] = e

        #VAEDecoderSegmentation
        if run_vae_decoder_enhancement:
            for c in cluster_counts:
                c_name, c = ("true", true_cc) if c == TRUE_CLUSTER_COUNT else (str(c), c)
                result[f"vae_enhanced_c{c_name}"] = VAEdecoder_enhancement.enhance_segmentation_final(args.exp_path, c)

        yield args, result



if __name__ == "__main__":
    data_path = "data/selected_coco_images"
    results_path = "results"
    coco_version = "coarse"
    no_prompts = True
    cluster_counts = [6, TRUE_CLUSTER_COUNT]
    slic_segment_counts = [100]
    differential_enhancements = [differential_enhancement.SCORE_ENCODER]
    #differential_enhancements = []
    run_vae_decoder_enhancement = False

    #if plot results, set to False
    headless = True    
    
    #perform the required segmentations for all of the coco files
    for i, (args, result) in enumerate(run_everything(data_path, results_path, coco_version, no_prompts, cluster_counts, slic_segment_counts, differential_enhancements, skip_missing_differential_files=run_vae_decoder_enhancement)):
        print (f"Run everything: Image {i}")
        #if not running on the server, show the results in matplotlib
        if not headless:
            fig, axs = plt.subplots(4, 3)
            axs = [a for b in axs for a in b]

            for (res_name, res), ax in zip(result.items(), axs):
                ax.imshow(res)
                ax.set_title(res_name)
                ax.axis('off')
            plt.show()
