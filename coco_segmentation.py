## This file is responsible for listing all images available in our sample of the COCO dataset (the selected images)

import run_ldm
import os


def get_image_description(image_number, file_path):
    """
    Get the description for a specific image number from a text file.

    Parameters:
    - image_number (str): The image number (filename) to retrieve the description for.
    - file_path (str): The path to the text file containing image descriptions.

    Returns:
    - str: The description for the specified image.
    """
    with open(file_path, 'r') as file:
        lines = file.readlines()

    for line in lines:
        parts = line.strip().split(':')
        if len(parts) == 2:
            current_image_number, description = parts
            if current_image_number.strip() == image_number:
                return description.strip()

    return f"No description found for image {image_number}"



class COCOFile:
    def __init__(self, data_path, results_path, path_to_files, filename, coco_version, prompt, no_prompts):
        self.filename = filename
        self.image = f"{path_to_files}/{filename}.jpg"
        self.true = f"{data_path}/val2017_annotated_{coco_version}/{filename}.png"
        self.exp_path = f"{results_path}/prompt_{coco_version}/{filename}"
        if no_prompts:
            self.exp_path = f"{results_path}/prompt_empty/{filename}"
            self.prompt = ""
        else: 
            self.prompt = prompt
        
        self.coco_version = coco_version

    def __str__(self):
        return f"COCOFile (image='{self.image}', true='{self.true}', exp_path='{self.exp_path}', prompt='{self.prompt}', coco_version='{self.coco_version}')"
    
    def __repr__(self):
        return str(self)


def get_coco_files(data_path, results_path, coco_version, no_prompts):
    path_to_prompts = f'{data_path}/prompts_{coco_version}.txt'
    path_to_files = f'{data_path}/val2017_original'
    list_of_images = os.listdir(path_to_files)

    for file_name in list_of_images: #tqdm(list_of_images, desc="Processing files", unit="file"):
        if file_name.endswith('.jpg'): #to handle .DS files for Mac users
            prompt = get_image_description(file_name, path_to_prompts)
            file_name = file_name.removesuffix('.jpg')
            yield COCOFile(data_path, results_path, path_to_files, file_name, coco_version, prompt, no_prompts)


if __name__ == "__main__":
    data_path = "data/selected_coco_images"
    coco_version = "coarse"
    results_path = "data/selected_coco_images"
    no_prompts = True
    for args in get_coco_files(data_path, results_path, coco_version, no_prompts):
        print (f"Running with args = {args}")