### The simple enhancement file. Attempt to upscale the clusters using either the nearest neigbor or linear interpolation

import kmeans_clustering
import numpy as np
import os

NEAREST_NEIGHBOR, LINEAR = 0, 1

def segmentation_voting(clusters, voting, region_segments = None):
    mult = 512 // clusters.shape[0]
    hmult = mult // 2

    no_regions = region_segments is None

    cluster_count = np.max(clusters)+1

    if no_regions:
        result = np.zeros((512, 512), np.int32)
    else:
        #for each slic segment, collect votes for each lowres segment. The segment most present in this area will be deemed to fill the whole area in the end 
        votes = np.zeros([np.max(region_segments)+1, cluster_count], dtype=np.int32)
    
    #go over all image pixels
    for x in range(512):
        for y in range(512):
            
            if voting == NEAREST_NEIGHBOR:
                #nearest neigbor - the value of the current pixel is equal to the nearest lowres segment - just add one vote for it
                lr_seg_nn = clusters[x // mult, y // mult]
                
                if no_regions:
                    result[x, y] = lr_seg_nn
                else:
                    slic_seg = region_segments[x, y]
                    votes[slic_seg, lr_seg_nn] += 1

            elif voting == LINEAR:
                #linear interpolation between the four surrounding pixels
                
                # compute indices of the surrounding pixels (for x and y axes, unclamped)
                x0_, y0_ = (x - hmult) // mult, (y - hmult) // mult
                x1_, y1_ = x0_ + 1, y0_ + 1
                
                #clamp indices to image size
                x0, y0, x1, y1 = max(0, x0_), max(0, y0_), min(x1_, 32-1), min(y1_, 32-1)
                #get lowres segment labels of the 4 surrounding pixels
                a, b, c, d = clusters[x0, y0], clusters[x1, y0], clusters[x0, y1], clusters[x1, y1]
                
                #compute coefficients for each side
                kx1, ky1 = x1_ * mult - (x - hmult), y1_ * mult - (y - hmult)
                kx2, ky2 = mult-kx1, mult-ky1
                
                # no regions - we compare the presence of the four neighboring pixels and select the cluster with the highest weight
                if no_regions:
                    votes = np.zeros([cluster_count])
                    votes[a] += kx1 * ky1
                    votes[b] += kx2 * ky1
                    votes[c] += kx1 * ky2
                    votes[d] += kx2 * ky2
                    result[x, y] = np.argmax(votes)
                else: # otherwise we perform linear interp on the slic segments, add votes to one of them
                    slic_seg = region_segments[x, y]
                    #add votes according to the linear interpolation
                    votes[slic_seg, a] += kx1 * ky1
                    votes[slic_seg, b] += kx2 * ky1
                    votes[slic_seg, c] += kx1 * ky2
                    votes[slic_seg, d] += kx2 * ky2
            else:
                print ("Invalid voting argument, must be set to either NEAREST_NEIGHBOR or LINEAR")
                return None
    
    #no regions -> result has already been computed, return it
    if no_regions: return result

    #use the votes to select the most popular lowres segment for each slic region    
    slic_to_lr = np.argmax(votes, axis=1)

    #construct the high resolution segmentation image
    full_segmentation = np.zeros((512, 512), dtype=np.int32)
    #go over all pixels
    for x in range(512):
        for y in range(512):
            #the seg pixel = set according to the color of the SLIC segment present here
            full_segmentation[x, y] = slic_to_lr[region_segments[x, y]]
    
    return full_segmentation



#Use nearest neighbors to enhance segmentation quality -> = using NNs to upscale the clusters image
def nearest_neighbors_enhancement(exp_path, cluster_count):
    result_fname = f"{exp_path}/nns_c{cluster_count}.npy"
    if os.path.exists(result_fname): return np.load(result_fname)

    clusters = np.load(kmeans_clustering.clusters_file(exp_path, cluster_count))
    result = segmentation_voting(clusters, NEAREST_NEIGHBOR, None)
    np.save(result_fname, result)

    return result


#Use linear interpolation (adapted for the integer image) to ehnance segmentation quality
def linear_enhancement(exp_path, cluster_count):
    result_fname = f"{exp_path}/linear_c{cluster_count}.npy"
    if os.path.exists(result_fname): return np.load(result_fname)

    clusters = np.load(kmeans_clustering.clusters_file(exp_path, cluster_count))
    result = segmentation_voting(clusters, LINEAR, None)
    np.save(result_fname, result)

    return result