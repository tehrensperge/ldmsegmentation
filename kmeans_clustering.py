##File containing methods to run K-means on self-attention

import run_ldm
import numpy as np
from sklearn.cluster import KMeans
import os
from src.segmentation_utils import save_dict, label_segmentation, load_text
from local_prompt_mixing.src.attention_based_segmentation import Segmentor 


def clusters_file(exp_path, cluster_count): return f"{exp_path}/clusters_{cluster_count}.npy"
def clusters_merged_file(exp_path, cluster_count): return f"{exp_path}/clusters_merged_{cluster_count}.npy"
def cluster2noun_file(exp_path, cluster_count): return f"{exp_path}/cluster2noun_{cluster_count}.pkl"


def clustering_done(exp_path, cluster_count):
    return all(map(os.path.exists, [clusters_file(exp_path, cluster_count), clusters_merged_file(exp_path, cluster_count), cluster2noun_file(exp_path, cluster_count)]))


def main(exp_path, cluster_count, no_prompts):
    if not run_ldm.all_output_filenames(exp_path, no_prompts):
        print ("You need to run 'run_segmentation.py' before attempting to peform KMeans clustering")

    if clustering_done(exp_path, cluster_count):
        return 

    #self attention is [32, 32, 1024] -> defining self attention of each pixel to each other pixel, in the latent space
    self_attention = np.load(run_ldm.self_attention_file(exp_path))

    cross_attention = None
    if not no_prompts: 
        cross_attention = np.load(run_ldm.cross_attention_file(exp_path))

    segmentor = Segmentor(self_attention, cross_attention, load_text(run_ldm.prompt_file(exp_path)), cluster_count)

    #run KMeans and save the clusters
    clusters = segmentor.cluster()
    np.save(clusters_file(exp_path, cluster_count), clusters)

    #load the cluster2noun
    if not no_prompts:
        cluster2noun = segmentor.cluster2noun(clusters)
        save_dict(cluster2noun_file(exp_path, cluster_count), cluster2noun)
        #merge the clusters associated with the same cluster noun and save them
        clusters_merged = label_segmentation(clusters, cluster2noun)
        np.save(clusters_merged_file(exp_path, cluster_count), clusters_merged)
    else:
        save_dict(cluster2noun_file(exp_path, cluster_count), {})
        np.save(clusters_merged_file(exp_path, cluster_count), clusters)
