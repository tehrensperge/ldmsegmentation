##OUR METHOD 2: uses Jacobian of the decoder as weight for how much pixel in image space depend on latent space

import os
from dataclasses import dataclass
import torch
from torchvision.utils import save_image
import matplotlib.pyplot as plt
import numpy as np
from local_prompt_mixing.src.diffusion_model_wrapper import get_stable_diffusion_model, get_stable_diffusion_config, DiffusionModelWrapper
from local_prompt_mixing.src.null_text_inversion import invert_image
from local_prompt_mixing.src.prompt_to_prompt_controllers import AttentionStore
from local_prompt_mixing.src.null_text_inversion import load_512
from tqdm import tqdm
from skimage.segmentation import mark_boundaries
from src import segmentation_utils
import run_ldm
import kmeans_clustering
import simple_enhancement

#Config copied from the original run_segmentation file. Everything is kept to ensure the model runs in the same way, the args are passed to another class as a whole
@dataclass
class SegmentationConfig:
    seed: int = 1111
    gpu_id: int = 0

    auth_token: str = ""
    low_resource: bool = True
    num_diffusion_steps: int = 50
    guidance_scale: float = 7.5
    max_num_words: int = 77
    
    prompt: str = "photo of a cat"
    exp_path: str = "segmentation_results_cat"
    

    num_segments: int = 5
    background_segment_threshold: float = 0.35


#supported operation modes
SCORE_DECODER, SCORE_ENCODER, SCORE_DECODER_SELECTIVE, SCORE_DECODER_LOWRES, SCORE_DECODER_LOWRES_SELECTIVE = 0, 1, 2, 3, 4
# -> how to obtain gradient values
# * decoder - decoder output w.r. to the latent space. Calls backprop 512*512*3 times, too slow (~50 hours)
# * encoder - encoder output w.r. to the image. Backprop 64*64*4 times, runs ~15-20 mins.
# * decoder_selective - decoder output w.r. to the latent space, but only on edge pixels according to the merged clusters. Approx 7 times faster than decoder, ~8 hours
# * decoder_lowres - use intermediate decoder layer (128x128) + only sample gradients instead of taking the sum. Takes 1.5 hours, output only 128x128.
# * decoder_lowres_selective - intermediate decoder layer + only boundary pixels. Runs ~15-20 mins, output only 128x128

score_fnames = ["decoder", "encoder", "decoder_selective", "decoder_lowres", "decoder_lowres_selective"]
#target image resolutions
score_resolutions = [512, 512, 512, 128, 128]


def result_file(exp_path, method, cluster_count): return f"{exp_path}/differential_{score_fnames[method]}_{cluster_count}.npy"

def all_result_files(exp_path, method, cluster_counts): return [result_file(exp_path, method, c) for c in cluster_counts]

def all_files_done(exp_path, method, cluster_counts): return all(map(os.path.exists, all_result_files(exp_path, method, cluster_counts)))


#Differential enhancement - try to improve segmentation quality using the jacobian of the encoder/decoder
#This file assumes that run_segmentation was already executed and all the results are present
def differential_enhancement(exp_path, score_method, save_scores, cluster_counts, verbose=True, skip_missing_differential_files=False):
    args = SegmentationConfig()

    #the filename to save scores as
    score_fname = score_fnames[score_method]
    #the resolution of the scores in the x & y dimension
    score_res = score_resolutions[score_method]


    #if the result already exists, return
    if all_files_done(exp_path, score_method, cluster_counts):
        if verbose: print ("All files already exist, returning loaded versions")
        return [np.load(f) for f in all_result_files(exp_path, score_method, cluster_counts)]
    else:
        if skip_missing_differential_files:
            return None

    #load the image to segment
    image = run_ldm.open_real_image(exp_path)
    
    #the filename of the segmentation
    differential_scores_file = f"{args.exp_path}/differential_scores_{score_fname}.npy"


    #all cluster files, loaded
    all_clusters = [np.load(kmeans_clustering.clusters_file(exp_path, c)) for c in cluster_counts]

    #if the segmentation file doesn't exist yet, compute it
    if not os.path.exists(differential_scores_file):
        

        #load the model
        ldm_stable = get_stable_diffusion_model(args)
        ldm_stable_config = get_stable_diffusion_config(args)

        #pass the model to the wrapper
        g_cpu = torch.Generator(device=ldm_stable.device).manual_seed(args.seed)
        controller = AttentionStore(ldm_stable_config["low_resource"])
        diffusion_model_wrapper = DiffusionModelWrapper(args, ldm_stable, ldm_stable_config, controller, generator=g_cpu)
        
        #load the latents and merged clusters from files
        latents = torch.tensor( np.expand_dims(np.load(run_ldm.all_latents_file(exp_path))[0], 0)).cuda()
        
        
        
        #if we should take the jacobian of the decoder
        if score_method == SCORE_DECODER:
            #load the latents from file, take the first one (least noisy)
            scores = diffusion_model_wrapper.compute_decoder_differential_scores(latents)
        #take the jacobian of the encoder
        elif score_method == SCORE_ENCODER:
            #compute the scores using the loaded image
            scores = diffusion_model_wrapper.compute_encoder_differential_scores(image)
        elif score_method == SCORE_DECODER_SELECTIVE:
            #get the list of interesting pixel positions (a position is interesting if it is interesting in any of the clusterings)
            positions = np.any([segmentation_utils.interesting_pixels_resized((512, 512), c) for c in all_clusters], 0)
            #create combinations of position, channel for R,G,B
            positions = np.array([[*pos, i] for i in range(3) for pos in positions])
            #compute the final scores at the interesting positions
            scores = diffusion_model_wrapper.compute_decoder_differential_scores(latents, positions)
        elif score_method == SCORE_DECODER_LOWRES:
            #compute low resolution decoder scores, 128x128
            scores = diffusion_model_wrapper.compute_decoder_lowres_differential_scores(latents)
        elif score_method == SCORE_DECODER_LOWRES_SELECTIVE:
            #compute interesting positions in 128x128 resolution (if it is the border in any of the clusterings)
            positions = np.any([segmentation_utils.interesting_pixels_resized((128, 128), c) for c in all_clusters], 0)
            #compute low resolution
            scores = diffusion_model_wrapper.compute_decoder_lowres_differential_scores(latents, positions)
        scores = scores.cpu().numpy()
        
        #save the computed scores
        if save_scores: np.save(differential_scores_file, scores)
    else:
        #load the scores if they exist instead
        scores = np.load(differential_scores_file)
    

    result = []
    for low_res_segmentation, cluster_count in zip(all_clusters, cluster_counts):
        result_fname = result_file(exp_path, score_method, cluster_count)

        if os.path.exists(result_fname):
            print (f"File already exists for cluster count = {cluster_count}, skipping.")
            continue

        #compute the count of segments
        seg_count = np.max(low_res_segmentation)+1
        
        #whether to use NN or linear to upscale low res segmentation from 32x32 to 64x64
        NEAREST_NEIGHBOR, LINEAR = 0, 1
        interpolation = LINEAR
        
        #votes for each pixel for each segment
        votes = np.zeros([score_res, score_res, seg_count])

        #for each pixel in the low res segmentation
        for x in tqdm(range(64)):
            for y in range(64):
                #when using nn voting - for each pixel, figure out how much it affects output pixels by using scores
                # add a vote to the class of the current pixel. (NN means blocks of 4 pixels will be using the same class)
                if interpolation == NEAREST_NEIGHBOR:
                    votes[:, :, low_res_segmentation[x//2, y//2]] += scores[:, :, x, y]
                elif interpolation == LINEAR:
                    #linear interpolation, hardcoded for 2x scaling up
                    #compute how much current pixel depends on the previous value and current value
                    kx = 0.25 if x % 2 == 0 else 0.75
                    ky = 0.25 if y % 2 == 0 else 0.75
                    
                    #edge cases - if on edge, we depend on one pixel only
                    if x == 0: kx = 0
                    if y == 0: ky = 0
                    if x == 63: kx = 1
                    if y == 63: ky = 1
                    
                    #compute indices of the first of four affected pixels. Others will have coords (x_+1, y_), (x_, y_+1), (x_+1, y_+1)
                    x_ = (x-1)//2
                    y_ = (y-1)//2
                    
                    #Add the same scores each time - but to different classes if neighboring pixels differ. Weights correspond to 2D linear interpolation
                    if kx != 0:
                        if ky != 0: votes[:, :, low_res_segmentation[x_, y_]] += kx * ky * scores[:, :, x, y]
                        if ky != 1: votes[:, :, low_res_segmentation[x_, y_+1]] += kx * (1-ky) * scores[:, :, x, y]
                    if kx != 1:
                        if ky != 0: votes[:, :, low_res_segmentation[x_+1, y_]] += (1-kx) * ky * scores[:, :, x, y]
                        if ky != 1: votes[:, :, low_res_segmentation[x_+1, y_+1]] += (1-kx) * (1-ky) * scores[:, :, x, y]
                else:
                    print ("Invalid interpolation")
                
        # for each pixel, select the most voted class
        segmentation = np.argmax(votes, -1)
        
        if segmentation.shape[0] != 512:
            segmentation = simple_enhancement.segmentation_voting(segmentation, simple_enhancement.LINEAR)

        result.append(segmentation)

        #save the computed (upscaled) segmentation
        np.save(result_fname, segmentation)

    return result