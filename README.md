# Precise Segmentation Using the Latent Space of LDMs

## Description
Code for the Deep Learning Project (ETH Autumn Semester 2023) _Precise Segmentation Using the Latent Space of LDMs_.

## Abstract
Generating precise segmentation masks for images
poses a crucial challenge in the field of computer
vision. While the main motivation of Latent
Diffusion Models (LDM) is the generation of highquality
images from a text prompt, the latent space
of the trained models can be used to perform the
segmentation of real images. We investigate different
methods for enhancing the resolution of
the latent segmentation in the zero-shot unsupervised
learning setting by combining it either with
image pixel information directly or by extracting
additional information from the Variational
Autoencoder (VAE) that is part of the LDM. We
publish our code and data on [GitLab](https://gitlab.ethz.ch/tehrensperge/ldmsegmentation).

## Setup
If you use the zip-version, make sure to use the GitLab version such that you have all the data.
To be on the safe side, you should follow the `README.md` inside the folder `local_prompt_mixing` to install all requirements to run the LDM and our code. However, we set up the notebook `easy_setup.ipynb` which - after you created a new conda environment - you can run and should get all necessary packages installed.

## Demo file
- `DEMO.ipynb`: we've already ran the LDM for you to produce the self-attention and latent representation. you can run the methods yourself as well as display the already computed segmentation maps

## Most important parameters tu run files below:
The following parameters can be set to run single files below. The `compute_scores.py` is the main file, but if you don't want to compute the scores itself, you can choose to run single files such as `run_everything.py` as well.

- data_path: path where true files are located
- results_path: path where result files of LDM and segmenation are stored
- coco_version: "coarse" or "fine". the paper discusses the COCO-Stuff-27 version which is "coarse". "fine" has more labels, results are comparable and not displayed in the paper
- no_prompts: True or False. paper mainly discusses True (no prompts used as input to LDM). if you want to match predicted clusters with cross-attention to true labels, use False
- cluster_counts: list with different K parameters used for K-means clustering of self-attention 
- slic_segment_counts: list containing different number of segments used for SLIC superpixels method
- differential_enhancements: list containing different versions of the second method presented in the paper (Jacobian of VAE). in the paper we only discuss the basic method differential_enhancement.SCORE_ENCODER since it's the most feasible to run
- run_vae_decoder: True or False. boolean if you want to run Method 3 from the paper. (VAE decoder)

## File structure
- `data`: contains files we tested our methods on. separate 'README.md' in folder explains how to download the whole COCO-Stuff
- `results`: since running the LDM repeatedly for the same image is computationally heavy, we store the attention files and further result files after running the LDM for the images in `data`

- `local_prompt_mixing`: original code from [Local Prompt Mixing](https://github.com/orpatashnik/local-prompt-mixing) paper. We added some methods to `src/diffusion_model_wrapper.py`. All other changes were minor.


- `compute_scores.py`: computes and displays the scores for the test files in `data`, using the `results` files and true COCO-Stuff-27 annotations

- `..._enhancement.py`-files: representing our 3 main methods.different stategies used to upscale the low-dimensional clustered self-attention file (see the report as well)

    - `simple_enhancement.py`: serves as baseline, uses nearest neighbors or linear interpolation as upscaling strategies
    - `SLIC_enhancement.py`: uses SLIC algorithm on real image to produce high-dimensional clusters and assigns each segment a class from low-dimensional cluster via voting
    - `differential_enhancement.py`: uses Jacobian of the decoder as weight for how much pixel in image space depend on latent space
    - `VAEdecoder_enhancement.py`: clusters image by slightly modifying the latent vector and look at difference that occured due to the modification after running it through the decoder

- `run_everything.py`: run LDM, segmentation and display results for all images in chosen folder
- `run_segmentation.py`: run LDM for a single image and save resulting attention files
- `coco_segmentation.py`: lists all files we compute our scores for, i.e. the subset of the COCO-Stuff-27 dataset, contained in the `data` folder

- `src`-folder:
    
    - helper files, mostly for the `DEMO.ipynb` which can't use some methods from above directly
    - `match_labels.py`: hungarian, argmax and cross attention matching to assign a COCO label to a cluster


## Acknowledgments 
We thank Enis Simar for the time he has taken to help us and for providing suggestions during the project finding phase.

This code builds on the code from [Local Prompt Mixing](https://github.com/orpatashnik/local-prompt-mixing).

## Contributors
Tim Ehrensperger, Matej Mrazek, Jon Nyffeler, Pablo Blasco Fernandez

## Remark for M1 Mac Users (mps instead of cpu)

### Change Device to MPS
Change the first lines of the get_stable_diffusion_model function in the file `local_prompt_mixing/src/diffusion_model_wrapper` to the following:
```
if torch.backends.mps.is_available():
        device = torch.device("mps")
    else:
        print ("MPS device not found.")
    #device = torch.device(f'cuda:{args.gpu_id}') if torch.cuda.is_available() else torch.device('cpu')
```
