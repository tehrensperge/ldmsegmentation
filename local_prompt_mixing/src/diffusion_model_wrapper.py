from typing import Optional, List

import numpy as np
import torch
from cv2 import dilate
from diffusers import DDIMScheduler, StableDiffusionPipeline
from tqdm import tqdm

from local_prompt_mixing.src.attention_based_segmentation import Segmentor
from local_prompt_mixing.src.attention_utils import show_cross_attention
from local_prompt_mixing.src.prompt_to_prompt_controllers import DummyController, AttentionStore


def get_stable_diffusion_model(args, load_scheduler = True):
    device = torch.device(f'cuda:{args.gpu_id}') if torch.cuda.is_available() else torch.device('cpu')
    if load_scheduler:
        scheduler = DDIMScheduler(beta_start=0.00085, beta_end=0.012, beta_schedule="scaled_linear", clip_sample=False, set_alpha_to_one=False)
        ldm_stable = StableDiffusionPipeline.from_pretrained("CompVis/stable-diffusion-v1-4", use_auth_token=args.auth_token, scheduler=scheduler).to(device)
    else:
        ldm_stable = StableDiffusionPipeline.from_pretrained("CompVis/stable-diffusion-v1-4", use_auth_token=args.auth_token).to(device)

    return ldm_stable

def get_stable_diffusion_config(args):
    return {
        "low_resource": args.low_resource,
        "num_diffusion_steps": args.num_diffusion_steps,
        "guidance_scale": args.guidance_scale,
        "max_num_words": args.max_num_words
    }


def generate_original_image(args, ldm_stable, ldm_stable_config, prompts, latent, uncond_embeddings):
    g_cpu = torch.Generator(device=ldm_stable.device).manual_seed(args.seed)
    controller = AttentionStore(ldm_stable_config["low_resource"])
    diffusion_model_wrapper = DiffusionModelWrapper(args, ldm_stable, ldm_stable_config, controller, generator=g_cpu)
    image, x_t, orig_all_latents, _ = diffusion_model_wrapper.forward(prompts,
                                                                      latent=latent,
                                                                      uncond_embeddings=uncond_embeddings)
    orig_mask = Segmentor(controller, prompts, args.num_segments, args.background_segment_threshold, background_nouns=args.background_nouns)\
        .get_background_mask(args.prompt.split(' ').index("{word}") + 1)
    average_attention = controller.get_average_attention()
    return image, x_t, orig_all_latents, orig_mask, average_attention


class DiffusionModelWrapper:
    def __init__(self, args, model, model_config, controller=None, prompt_mixing=None, generator=None, export_dir=None):
        self.args = args
        self.model = model
        self.model_config = model_config
        self.controller = controller
        if self.controller is None:
            self.controller = DummyController()
        self.prompt_mixing = prompt_mixing
        self.device = model.device
        self.generator = generator

        self.height = 512
        self.width = 512

        self.diff_step = 0
        self.register_attention_control()
        
        if export_dir is not None:
            def get_activation(name):
                def hook(model, input, output):
                    np.save(f"{export_dir}/{name}.npy", output.detach().cpu())
                    #activation[name] = output.detach()
                return hook
            self.model.vae.decoder.up_blocks[0].resnets[0].register_forward_hook(get_activation("up0_resnet0"))
            self.model.vae.decoder.up_blocks[0].resnets[1].register_forward_hook(get_activation("up0_resnet1"))
            self.model.vae.decoder.up_blocks[0].resnets[2].register_forward_hook(get_activation("up0_resnet2"))
            self.model.vae.decoder.up_blocks[0].upsamplers[0].register_forward_hook(get_activation("up0_up0"))
            
            self.model.vae.decoder.up_blocks[1].resnets[0].register_forward_hook(get_activation("up1_resnet0"))
            self.model.vae.decoder.up_blocks[1].resnets[1].register_forward_hook(get_activation("up1_resnet1"))
            self.model.vae.decoder.up_blocks[1].resnets[2].register_forward_hook(get_activation("up1_resnet2"))
            self.model.vae.decoder.up_blocks[1].upsamplers[0].register_forward_hook(get_activation("up1_up0"))
            
            self.model.vae.decoder.up_blocks[2].resnets[0].register_forward_hook(get_activation("up2_resnet0"))
            self.model.vae.decoder.up_blocks[2].resnets[1].register_forward_hook(get_activation("up2_resnet1"))
            self.model.vae.decoder.up_blocks[2].resnets[2].register_forward_hook(get_activation("up2_resnet2"))
            self.model.vae.decoder.up_blocks[2].upsamplers[0].register_forward_hook(get_activation("up2_up0"))
            
            self.model.vae.decoder.up_blocks[3].resnets[0].register_forward_hook(get_activation("up3_resnet0"))
            self.model.vae.decoder.up_blocks[3].resnets[1].register_forward_hook(get_activation("up3_resnet1"))
            self.model.vae.decoder.up_blocks[3].resnets[2].register_forward_hook(get_activation("up3_resnet2"))
            
            self.model.vae.decoder.mid_block.attentions[0].register_forward_hook(get_activation("mid_attention0"))
            self.model.vae.decoder.mid_block.resnets[0].register_forward_hook(get_activation("mid_resnet0"))
            self.model.vae.decoder.mid_block.resnets[1].register_forward_hook(get_activation("mid_resnet1"))
            
            self.model.vae.decoder.conv_out.register_forward_hook(get_activation("conv_out"))
        
        


    def diffusion_step(self, latents, context, t, other_context=None):
        if self.model_config["low_resource"]:
            self.uncond_pred = True
            noise_pred_uncond = self.model.unet(latents, t, encoder_hidden_states=(context[0], None))["sample"]
            self.uncond_pred = False
            noise_prediction_text = self.model.unet(latents, t, encoder_hidden_states=(context[1], other_context))["sample"]
        else:
            latents_input = torch.cat([latents] * 2)
            noise_pred = self.model.unet(latents_input, t, encoder_hidden_states=(context, other_context))["sample"]
            noise_pred_uncond, noise_prediction_text = noise_pred.chunk(2)
        noise_pred = noise_pred_uncond + self.model_config["guidance_scale"] * (noise_prediction_text - noise_pred_uncond)
        latents = self.model.scheduler.step(noise_pred, t, latents)["prev_sample"]
        latents = self.controller.step_callback(latents)
        return latents


    def latent2image(self, latents):
        latents = 1 / 0.18215 * latents
        image = self.model.vae.decode(latents)['sample']
        image = (image / 2 + 0.5).clamp(0, 1)
        image = image.cpu().permute(0, 2, 3, 1).detach().numpy()
        image = (image * 255).astype(np.uint8)
        return image


    def compute_decoder_differential_scores(self, latents, position_img = None):
        #differential scores = we want to figure out how much do output values depend on the latent space = the jacobian of input w.r. to the output
        # -> Just compute the jacobian of the decoder, right?
        # Is way too slow (we need to perform the backward pass 512*512*3 times, which takes 2 days on a 3090. Code only left as a proof of concept)
        scores = torch.zeros([512, 512, 64, 64], dtype=torch.float32)
        
        #when positions are not specified, run for all of them
        if position_img is None: position_img = np.ones((512, 512), dtype=bool)
        
        scores = scores.cuda()
        
        #require gradient on the latents
        latents.requires_grad = True
        
        #rescale according to the latent2image method above and run the forward pass
        latents2 = 1 / 0.18215 * latents
        image = self.model.vae.decode(latents2)['sample'] / 2 + 0.5
                
        #go over all output pixels and channels
        with tqdm(total=3*np.sum(np.where(position_img, 1, 0))) as bar:
            for x in range(512):
                for y in range(512):
                    if position_img[x, y]:
                        for c in range(3):
                            #we want to figure out how much one pixel depends on the latent space -> compute gradient with respect to it
                            grd = torch.zeros((1, 3, 512, 512)).cuda()
                            grd[0, c, x, y] = 1
                            
                            #perform the backward pass
                            image.backward(gradient=grd, retain_graph=True)

                            #we only care how much the pixel depends on a position in the latent space, not the channels - take the abs and sum everything
                            scores[x, y] += torch.sum(torch.abs(latents.grad[0]), 0)
                            #reset the gradients for the next pass
                            latents.grad.zero_()
                            
                            #update the progress bar
                            bar.update()
                    else:
                        scores[x, y, x // 8, y // 8] = 1 


        #return the final differential scores
        return scores
    
    
    
    def compute_decoder_lowres_differential_scores(self, latents, position_img = None):
        #lowres differential scores = we want to figure out how much do output values depend on the latent space = the jacobian of input w.r. to the output
        # -> Just compute the jacobian of the decoder, right?
        # Way too slow by default. For this reason we try to use one of the decoder middle layers instead of the output
        # We use the 128x128 layer here
        resolution = 128
        #how much do we upscale
        mult = resolution // 64
        
        #if no positions are specified, run on all of them
        if position_img is None: position_img = np.ones((resolution, resolution), bool)

        #score for each combination of input pixel & output pixel
        scores = torch.zeros([resolution, resolution, 64, 64], dtype=torch.float32)
        
        scores = scores.cuda()
        
        #require gradient on the latents
        latents.requires_grad = True
        
        
        def compute_grads(model, input, output):
            samples = 4
            c = output.shape[1]
            grd_masks = torch.where(torch.rand((samples, c), device="cuda") > 0.5, 1.0, 0.0)
            #go over all output pixels and channels
            with tqdm(total=samples*np.sum(np.where(position_img, 1, 0))) as bar:
                for x in range(resolution):
                    for y in range(resolution):
                        #if we are interested in computing the gradient for this position
                        if position_img[x, y]:
                            #we don't want to compute the gradient for each pixel, it would take too long (O(num of channels))
                            #instead we take random samples with approx. half of the positions ignored at random, take the abs of the results and sum
                            for sample in range(samples):
                                #we want to figure out how much one pixel depends on the latent space -> compute gradient with respect to it
                                grd = torch.zeros((1, c, resolution, resolution)).cuda()

                                #grd = grd_masks[sample]#torch.where(torch.rand((1, c, resolution, resolution), device="cuda") > 0.5, 1.0, 0.0)
                                grd[0, :, x, y] = grd_masks[sample]
                                
                                #perform the backward pass
                                output.backward(gradient=grd, retain_graph=True)

                                #we only care how much the pixel depends on a position in the latent space, not the channels - take the abs and sum everything
                                scores[x, y] += torch.sum(torch.abs(latents.grad[0]), 0)
                                #reset the gradients for the next pass
                                latents.grad.zero_()
                                
                                #update the progress bar
                                bar.update()
                        else:
                            #not interested in gradient - pixel only depends on the nearest neighbor in latent space
                            scores[x, y, x // mult, y // mult] = 1
        #add a forward hook for one of the model layers that will compute all the scores
        hook_handle = self.model.vae.decoder.up_blocks[1].resnets[2].register_forward_hook(compute_grads)
        
        #rescale according to the latent2image method above and run the forward pass
        #this will invoke the hook above
        latents2 = 1 / 0.18215 * latents
        self.model.vae.decode(latents2)['sample']
        
        #remove the gradient-computing hook registered above
        hook_handle.remove()
        
        #return the final differential scores
        return scores
    
    
    
    

    def compute_encoder_differential_scores(self, image):
        #differential scores = we want to figure out how much do output values depend on the latent space = the jacobian of input w.r. to the output
        # It would be ideal to use the decoder (compute how much pixel values depend on latent space), but that is way too slow
        # So we try to approximate this with the encoder (compute how much latent space depends on pixel values)
        scores = torch.zeros([512, 512, 64, 64], dtype=torch.float32, device="cuda")
        
        #code inspired by null_test_inversion.py - class NullInversion: image2latent method
        
        # convert image to mode input space - [0, 255] to [-1, 1] distribution
        image = torch.from_numpy(image).float() / 127.5 - 1
        #swap [w, h, c] to torch representation [c, w, h]; add batch dimension, move to GPU
        image = image.permute(2, 0, 1).unsqueeze(0).to(self.model.device)
        
        #we require the gradient of the image to be saved
        image.requires_grad = True
        
        #perform the forward pass - obtain latents
        latents = self.model.vae.encode(image)['latent_dist'].mean * 0.18215
        
        #print a progress bar
        with tqdm(total=64*64*4) as bar:
            #go over all output pixels
            for x in range(64):
                for y in range(64):
                    for c in range(4):
                        #set the gradient of the latent layer just to the 1 pixel on - compute gradient with respect to 1 output value
                        grd = torch.zeros((1, 4, 64, 64)).cuda()
                        grd[0, c, x, y] = 1
                        
                        #perform the backward pass
                        latents.backward(gradient=grd, retain_graph=True)
            
                        #add scores - take the absolute value of the gradient, sum over the color channels. (we don't care in which direction a value depends, only how much)
                        scores[:, :, x, y] += torch.sum(torch.abs(image.grad[0]), 0)
                        
                        #set gradient to zero, prepare for next backward pass
                        image.grad.zero_()
                        
                        #update the progress bar
                        bar.update()
        
        #return the final differential scores
        return scores



    def init_latent(self, latent, batch_size):
        if latent is None:
            latent = torch.randn(
                (1, self.model.unet.in_channels, self.height // 8, self.width // 8),
                generator=self.generator, device=self.model.device
            )
        latents = latent.expand(batch_size,  self.model.unet.in_channels, self.height // 8, self.width // 8).to(self.device)
        return latent, latents


    def register_attention_control(self):
        def ca_forward(model_self, place_in_unet):
            to_out = model_self.to_out
            if type(to_out) is torch.nn.modules.container.ModuleList:
                to_out = model_self.to_out[0]
            else:
                to_out = model_self.to_out

            def forward(x, context=None, mask=None):
                batch_size, sequence_length, dim = x.shape
                h = model_self.heads
                q = model_self.to_q(x)
                is_cross = context is not None
                context = context if is_cross else (x, None)

                k = model_self.to_k(context[0])
                if is_cross and self.prompt_mixing is not None:
                    v_context = self.prompt_mixing.get_context_for_v(self.diff_step, context[0], context[1])
                    v = model_self.to_v(v_context)
                else:
                    v = model_self.to_v(context[0])

                q = model_self.reshape_heads_to_batch_dim(q)
                k = model_self.reshape_heads_to_batch_dim(k)
                v = model_self.reshape_heads_to_batch_dim(v)

                sim = torch.einsum("b i d, b j d -> b i j", q, k) * model_self.scale

                if mask is not None:
                    mask = mask.reshape(batch_size, -1)
                    max_neg_value = -torch.finfo(sim.dtype).max
                    mask = mask[:, None, :].repeat(h, 1, 1)
                    sim.masked_fill_(~mask, max_neg_value)

                # attention, what we cannot get enough of
                attn = sim.softmax(dim=-1)
                if self.enbale_attn_controller_changes:
                    attn = self.controller(attn, is_cross, place_in_unet)
                
                if is_cross and self.prompt_mixing is not None and context[1] is not None:
                    attn = self.prompt_mixing.get_cross_attn(self, self.diff_step, attn, place_in_unet, batch_size)

                if not is_cross and (not self.model_config["low_resource"] or not self.uncond_pred) and self.prompt_mixing is not None:
                    attn = self.prompt_mixing.get_self_attn(self, self.diff_step, attn, place_in_unet, batch_size)

                out = torch.einsum("b i j, b j d -> b i d", attn, v)
                out = model_self.reshape_batch_dim_to_heads(out)
                return to_out(out)

            return forward

        def register_recr(net_, count, place_in_unet):
            if net_.__class__.__name__ == 'CrossAttention':
                net_.forward = ca_forward(net_, place_in_unet)
                return count + 1
            elif hasattr(net_, 'children'):
                for net__ in net_.children():
                    count = register_recr(net__, count, place_in_unet)
            return count

        cross_att_count = 0
        sub_nets = self.model.unet.named_children()
        for net in sub_nets:
            if "down" in net[0]:
                cross_att_count += register_recr(net[1], 0, "down")
            elif "up" in net[0]:
                cross_att_count += register_recr(net[1], 0, "up")
            elif "mid" in net[0]:
                cross_att_count += register_recr(net[1], 0, "mid")
        self.controller.num_att_layers = cross_att_count


    def get_text_embedding(self, prompt: List[str], max_length=None, truncation=True):
        text_input = self.model.tokenizer(
            prompt,
            padding="max_length",
            max_length=self.model.tokenizer.model_max_length if max_length is None else max_length,
            truncation=truncation,
            return_tensors="pt",
        )
        text_embeddings = self.model.text_encoder(text_input.input_ids.to(self.device))[0]
        max_length = text_input.input_ids.shape[-1]
        return text_embeddings, max_length


    @torch.no_grad()
    def forward(self, prompt: List[str], latent: Optional[torch.FloatTensor] = None,
                other_prompt: List[str] = None, post_background = False, orig_all_latents = None, orig_mask = None,
                uncond_embeddings=None, start_time=51, return_type='image'):
        self.enbale_attn_controller_changes = True
        batch_size = len(prompt)

        text_embeddings, max_length = self.get_text_embedding(prompt)
        if uncond_embeddings is None:
            uncond_embeddings_, _ = self.get_text_embedding([""] * batch_size, max_length=max_length, truncation=False)
        else:
            uncond_embeddings_ = None

        other_context = None
        if other_prompt is not None:
            other_text_embeddings, _ = self.get_text_embedding(other_prompt)
            other_context = other_text_embeddings

        latent, latents = self.init_latent(latent, batch_size)
        
        # set timesteps
        self.model.scheduler.set_timesteps(self.model_config["num_diffusion_steps"])
        all_latents = []

        object_mask = None
        self.diff_step = 0
        for i, t in enumerate(tqdm(self.model.scheduler.timesteps[-start_time:])):
            if uncond_embeddings_ is None:
                context = [uncond_embeddings[i].expand(*text_embeddings.shape), text_embeddings]
            else:
                context = [uncond_embeddings_, text_embeddings]
            if not self.model_config["low_resource"]:
                context = torch.cat(context)

            self.down_cross_index = 0
            self.mid_cross_index = 0
            self.up_cross_index = 0
            latents = self.diffusion_step(latents, context, t, other_context)

            if post_background and self.diff_step == self.args.background_blend_timestep:
                object_mask = Segmentor(self.controller,
                                        prompt,
                                        self.args.num_segments,
                                        self.args.background_segment_threshold,
                                        background_nouns=self.args.background_nouns)\
                    .get_background_mask(self.args.prompt.split(' ').index("{word}") + 1)
                self.enbale_attn_controller_changes = False
                mask = object_mask.astype(np.bool8) + orig_mask.astype(np.bool8)
                mask = torch.from_numpy(mask).float().cuda()
                shape = (1, 1, mask.shape[0], mask.shape[1])
                mask = torch.nn.Upsample(size=(64, 64), mode='nearest')(mask.view(shape))
                mask_eroded = dilate(mask.cpu().numpy()[0, 0], np.ones((3, 3), np.uint8), iterations=1)
                mask = torch.from_numpy(mask_eroded).float().cuda().view(1, 1, 64, 64)
                latents = mask * latents + (1 - mask) * orig_all_latents[self.diff_step]

            all_latents.append(latents)
            self.diff_step += 1

        if return_type == 'image':
            image = self.latent2image(latents)
        else:
            image = latents
        
        return image, latent, all_latents, object_mask
    
    
    def show_last_cross_attention(self, res: int, from_where: List[str], prompts, select: int = 0):
        show_cross_attention(self.controller, res, from_where, prompts, tokenizer=self.model.tokenizer, select=select)