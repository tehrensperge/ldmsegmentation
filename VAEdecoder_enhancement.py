#OUR METHOD 3: clusters image by slightly modifying the latent vector and look at difference that occured due to the modification after running it through the decoder


# Imports
import os
from dataclasses import dataclass
import torch
from torchvision.utils import save_image
import numpy as np
from local_prompt_mixing.src.diffusion_model_wrapper import get_stable_diffusion_model, get_stable_diffusion_config, DiffusionModelWrapper
from local_prompt_mixing.src.null_text_inversion import invert_image
from local_prompt_mixing.src.prompt_to_prompt_controllers import AttentionStore
from local_prompt_mixing.src.null_text_inversion import load_512
from skimage.util import img_as_float
from skimage import io
import scipy
import run_ldm
import kmeans_clustering
from src.segmentation_utils import load_512, load_text


###### Set up the diffusion model #######################
# Config copied from the original run_segmentation file. Everything is kept to ensure the model runs in the same way, the args are passed to another class as a whole
@dataclass
class SegmentationConfig():
    seed: int = 1111
    gpu_id: int = 0
    
    auth_token: str = ""
    low_resource: bool = True
    num_diffusion_steps: int = 50
    guidance_scale: float = 7.5
    max_num_words: int = 77
    
    num_segments: int = 5
    background_segment_threshold: float = 0.35

def augment_latent_representation(latent_z, clustered_image, nr_cluster):
    """
    Changes the latent space representation of the image slightly in the area of the specified cluster.

    :param latent_z: np.array() of shape (64, 64, 4). latent representation of the image. will be passed through VAE Decoder
    :param clustered_image: np.array() of shape (64, 64) of values {0, 1, .., nr_cluster}. encodes for each pixel in latent space what cluster it belongs to
    :param nr_cluster: int. the area of which of the clusters should be changed

    :return np.array() of shape (64, 64, 4). augmented latent space representation
    """

    latent_modified = np.copy(latent_z)
    idx = np.where(clustered_image==nr_cluster)
    latent_modified[idx[0], idx[1], 2] = latent_z[idx[0], idx[1], 2] + 1 # for the area of the specified cluster channel 2 will be increased by 1

    return latent_modified


#INPUT: latent_seg 32x32, original_image
def enhance_segmentation(latent_z, latent_seg, original_image, real_image_path, prompt):
    """
    :param latent_seg: np.array() of shape (32, 32). clustered image based on latent attention
    """

    # set up the model (need VAE part)
    args = SegmentationConfig()
    custom_args = {'prompt':prompt, 'real_image_path': real_image_path}
    for key, arg in custom_args.items():
        setattr(args, key, arg)

    #load the model
    ldm_stable = get_stable_diffusion_model(args)
    ldm_stable_config = get_stable_diffusion_config(args)

    #pass the model to the wrapper
    g_cpu = torch.Generator(device=ldm_stable.device).manual_seed(args.seed)
    controller = AttentionStore(ldm_stable_config["low_resource"])
    diffusion_model_wrapper = DiffusionModelWrapper(args, ldm_stable, ldm_stable_config, controller, generator=g_cpu)

    # Upsample the segmentation from 32x32 to 64x64
    upsampled_seg = scipy.ndimage.zoom(latent_seg, 2, order=0)

    # Initialize assignment matrix
    h, w, d = original_image.shape
    nr_clusters = len(np.unique(upsampled_seg)) # in case other method than K-means with flexible number of clusters is used
    assignment_matrix = np.zeros((h, w, nr_clusters))

    # compute image generated after passing through the decoder
    original_latent_to_decode = np.moveaxis(latent_z, 2, 0)
    original_latent_to_decode = torch.from_numpy(np.expand_dims(original_latent_to_decode, 0)).float().cuda()
    original_latent_to_decode.requires_grad = False
    original_image_decoded = diffusion_model_wrapper.latent2image(original_latent_to_decode)[0]/255

    # for each cluster we augment the area in the latent representation, pass it through the VAE Decoder and compute
    # the difference to the original image. We then find the "redness" of the difference and use it as a measure of 
    # how likely each pixel corresponds to the cluster
    for cluster in range(nr_clusters):
        augmented_latent = augment_latent_representation(latent_z, upsampled_seg, cluster)
        # change to right shape for the VAE Decoder
        augmented_latent = np.moveaxis(augmented_latent, 2, 0)
        augmented_latent = torch.from_numpy(np.expand_dims(augmented_latent, 0)).float().cuda()
        augmented_latent.requires_grad = False

        # pass the augmented latent representation through the VAE Decoder
        augmented_image = diffusion_model_wrapper.latent2image(augmented_latent)
        difference = original_image_decoded-augmented_image[0]/255

        # compute the "redness" of the difference of augmented and original image, which signifies likeliness of belonging to the cluster
        redness = difference[:,:,0]- (difference[:,:,1]+difference[:,:,2])/2
        assignment_matrix[:,:, cluster] = scipy.ndimage.filters.gaussian_filter(redness, 2, mode='constant') # smooth out to reduce noise
        
    # the final segmentation consists of choosing the most likely cluster for each pixel
    augmented_segmentation = np.argmax(assignment_matrix, 2)
    return augmented_segmentation


def enhance_segmentation_final(exp_path, cluster_count):
    target_file = f"{exp_path}/vae_segmentation_{cluster_count}.npy"
    if os.path.exists(target_file): return np.load(target_file)

    latent_z = np.load(run_ldm.all_latents_file(exp_path))[0].transpose((1, 2, 0))
    clusters = np.load(kmeans_clustering.clusters_file(exp_path, cluster_count))
    orig_image_path = run_ldm.real_image_file(exp_path)
    real_image = load_512(orig_image_path)
    result = enhance_segmentation(latent_z, clusters, real_image, orig_image_path, load_text(run_ldm.prompt_file(exp_path)))

    np.save(target_file, result)
    return result